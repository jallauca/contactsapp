//
//  ViewController.swift
//  ContactsApp
//
//  Created by Jaime Allauca on 7/2/20.
//  Copyright © 2020 Jaime Allauca. All rights reserved.
//

import Foundation

struct Contact: Decodable {
    enum CodingKeys: String, CodingKey {
        case company, email, phone, address, age, picture, name
        case id = "_id"
     }

    let id: String
    let name: Name
    let company: String
    let email: String
    let phone: String
    let address: String
    let age: Int
    let picture: URL
}

struct Name: Decodable {
    let first: String
    let last: String
}
