//
//  ViewController.swift
//  ContactsApp
//
//  Created by Jaime Allauca on 7/2/20.
//  Copyright © 2020 Jaime Allauca. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var contacts: [Contact] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.contacts = ContactRepository().load()
    }
}

